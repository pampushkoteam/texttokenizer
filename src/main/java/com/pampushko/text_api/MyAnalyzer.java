package com.pampushko.text_api;

import org.apache.lucene.analysis.Analyzer;

/**
 * Created by algernon on 4/19/17.
 */
public class MyAnalyzer extends Analyzer
{
	@Override
	protected TokenStreamComponents createComponents(String fieldName)
	{
		return new TokenStreamComponents(new org.apache.lucene.analysis.standard.StandardTokenizer());
	}
}
