package com.pampushko.text_api;

import com.fasterxml.jackson.databind.node.ArrayNode;
import org.rapidoid.annotation.Controller;
import org.rapidoid.annotation.POST;
import org.rapidoid.http.MediaType;
import org.rapidoid.http.Req;
import org.rapidoid.http.Resp;

/**
 *
 */
@Controller(value = "text")
public class TokenizeController
{
	@POST(value = "words")
	public Resp tokenizeText(Req req, Resp resp)
	{
		String text = new String(req.body());
		resp.contentType(MediaType.JSON);
		ArrayNode jsonArrayNode = TokenizeExpert.tokenizeText(text);
		resp.result(jsonArrayNode);
		return resp;
	}
}
