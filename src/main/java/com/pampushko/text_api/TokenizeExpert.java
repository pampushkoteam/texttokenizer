package com.pampushko.text_api;

import com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.rapidoid.data.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.lang.invoke.MethodHandles;

/**
 *
 */
public class TokenizeExpert
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public static ArrayNode tokenizeText(String text)
	{
		ArrayNode resultJsonArrayNode = JSON.newMapper().createArrayNode();
		
		//Version matchVersion = Version.LUCENE_XY; // Substitute desired Lucene version for XY
		MyAnalyzer analyzer = new MyAnalyzer();
		TokenStream stream = analyzer.tokenStream("field", new StringReader(text));
		
		// get the CharTermAttribute from the TokenStream
		CharTermAttribute termAtt = stream.addAttribute(CharTermAttribute.class);
		try
		{
			stream.reset();
			
			// print all tokens until stream is exhausted
			while (stream.incrementToken())
			{
				//System.out.println(termAtt.getClass());
				String word = termAtt.toString();
				log.info(word);
				resultJsonArrayNode.add(word);
			}
			
			stream.end();
		}
		catch (IOException ex)
		{
			log.error("IO Error of Stream!", ex.getStackTrace());
		}
		finally
		{
			try
			{
				if (stream != null)
				{
					stream.close();
				}
			}
			catch (IOException ex)
			{
				log.error("Stream not close!", ex.getStackTrace());
			}
		}
		return resultJsonArrayNode;
	}
}
